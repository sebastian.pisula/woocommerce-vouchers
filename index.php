<?php
/**
 * Plugin Name: WooCommerce Vouchers
 * Plugin URI: https://wpdesk.pl
 * Description:
 * Version: 1.0.0
 * Author: Sebastian Pisula
 * Author URI: mailto:sebastian.pisula@gmail.com
 * Text Domain: woocommerce-vouchers
 * Domain Path: /languages/
 * WC requires at least: 3.9.1
 * WC tested up to: 3.9.1
 */

namespace IC\WooCommerce\Voucher;

use IC\WooCommerce\Voucher\Module\Order;
use IC\WooCommerce\Voucher\Module\Product;
use IC\WooCommerce\Voucher\Module\Settings;
use IC\WooCommerce\Voucher\Module\Voucher;

defined( 'ABSPATH' ) || exit;

define( 'WC_V_VERSION', '1.0.0' );
define( 'WC_V_PLUGIN_FILE', __FILE__ );

include 'vendor/autoload.php';

/**
 * Class Plugin
 *
 * @package IC\WooCommerce\Voucher
 */
class Plugin {
	/**
	 * Register hooks.
	 */
	public function add_hooks() {
		add_action( 'woocommerce_init', [ $this, 'init_plugin' ] );
	}

	/**
	 * Init plugin.
	 */
	public function init_plugin() {
		( new Order() )->add_hooks();
		( new Product() )->add_hooks();
		( new Voucher() )->add_hooks();
		( new Settings() )->add_hooks();

		load_plugin_textdomain( 'woocommerce-vouchers', false, plugin_basename( dirname( WC_V_PLUGIN_FILE ) ) . '/languages' );
	}
}

( new Plugin() )->add_hooks();