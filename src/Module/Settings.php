<?php

namespace IC\WooCommerce\Voucher\Module;

/**
 * Class Settings
 *
 * @package IC\WooCommerce\Voucher\Module
 */
class Settings {
	/**
	 * Register hooks.
	 */
	public function add_hooks() {
		add_filter( 'plugin_action_links', [ $this, 'add_settings_link' ], 10, 4 );
		add_filter( 'woocommerce_get_settings_products', [ $this, 'get_settings' ], 50, 2 );
		add_filter( 'woocommerce_get_sections_products', [ $this, 'add_woocommerce_settings_tab' ], 50 );
	}

	/**
	 * @param string[] $actions
	 * @param string   $plugin_file
	 * @param array    $plugin_data
	 * @param string   $context
	 *
	 * @return string[]
	 */
	public function add_settings_link( $actions, $plugin_file, $plugin_data, $context ) {
		if ( $plugin_file === plugin_basename( WC_V_PLUGIN_FILE ) ) {
			$url = add_query_arg( [
				'page'    => 'wc-settings',
				'tab'     => 'products',
				'section' => 'vouchers'
			], admin_url( 'admin.php' ) );

			$settings_link = sprintf( "<a href=\"%s\">%s</a>", esc_url( $url ), __( 'Settings', '' ) );

			array_unshift( $actions, $settings_link );
		}

		return $actions;
	}

	public function get_settings( $settings, $current_section ) {
		if ( 'vouchers' !== $current_section ) {
			return $settings;
		}

		return array(
			[
				'name' => 'Voucher Configuration',
				'type' => 'title',
				'desc' => '',
				'id'   => 'voucher_defaults',
			],

			[
				'name'     => 'Send Vouchers as',
				'type'     => 'select',
				'desc'     => '',
				'default'  => 'link',
				'id'       => 'wc_vouchers_sent_as',
				'desc_tip' => true,
				'options'  => [
					'attachment' => 'Attachment',
					'link'       => 'Link to PDF',
				]
			],

			[ 'type' => 'sectionend', 'id' => 'voucher_defaults' ],
		);
	}

	/**
	 * Add settings tab to WooCommerce.
	 *
	 * @param array $sections
	 *
	 * @return array
	 */
	public function add_woocommerce_settings_tab( $sections ) {
		$sections['vouchers'] = 'Vouchers';

		return $sections;
	}
}