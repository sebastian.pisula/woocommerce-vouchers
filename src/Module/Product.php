<?php

namespace IC\WooCommerce\Voucher\Module;


use IC\WooCommerce\Voucher\Module\Product\Settings;

/**
 * Class Product
 *
 * @package IC\WooCommerce\Voucher\Module
 */
class Product {
	/**
	 * Register hooks.
	 */
	public function add_hooks() {
		( new Settings() )->add_hooks();
	}
}