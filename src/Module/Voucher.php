<?php

namespace IC\WooCommerce\Voucher\Module;

use IC\WooCommerce\Voucher\Lib\Helper;
use WP_Query;

/**
 * Class Voucher
 *
 * @package IC\WooCommerce\Voucher\Module
 */
class Voucher {
	/**
	 * Register hooks.
	 */
	public function add_hooks() {
		add_action( 'init', [ $this, 'register_post_type' ] );
		add_action( 'pre_get_posts', [ $this, 'filter_orders_by_voucher' ] );
		add_action( 'restrict_manage_posts', [ $this, 'add_voucher_filter' ] );

		add_action( 'admin_enqueue_scripts', [ $this, 'add_admin_scripts' ] );
	}

	/**
	 *
	 */
	public function register_post_type() {
		register_post_type( 'voucher-template', [
			'label'               => __( 'Voucher Template', 'text_domain' ),
			'labels'              => [
				'name'          => _x( 'Voucher Templates', 'Post Type General Name', 'text_domain' ),
				'singular_name' => _x( 'Voucher Template', 'Post Type Singular Name', 'text_domain' ),
				'menu_name'     => __( 'Vouchers', 'text_domain' ),
			],
			'supports'            => [ 'title', 'editor', 'revisions' ],
			'hierarchical'        => false,
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 5,
			'menu_icon'           => 'dashicons-tickets-alt',
			'show_in_admin_bar'   => false,
			'show_in_nav_menus'   => false,
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'rewrite'             => false,
			'capability_type'     => 'post',
			'show_in_rest'        => true,
		] );
	}

	/**
	 * @param WP_Query $q
	 */
	public function filter_orders_by_voucher( $q ) {
		if ( ! is_admin() | ! $q->is_main_query() ) {
			return;
		}

		$voucher = isset( $_GET['_voucher'] ) ? $_GET['_voucher'] : '';

		if ( ! $voucher ) {
			return;
		}

		$meta_query = $q->get( 'meta_query', [] );

		$meta_query[] = [
			'key'   => '_voucher',
			'value' => $voucher,
		];

		$q->set( 'meta_query', $meta_query );
	}

	/**
	 *
	 */
	public function add_admin_scripts() {
		$screen = get_current_screen();

		if ( $screen->id !== 'product' ) {
			return;
		}

		$file = 'assets/admin.js';
		wp_enqueue_script( 'woocommerce-vouchers', Helper::get_plugin_url( $file ), [ 'jquery' ], WC_V_VERSION, true );
	}

	/**
	 *
	 */
	public function add_voucher_filter() {
		global $typenow;

		$voucher = isset( $_GET['_voucher'] ) ? $_GET['_voucher'] : '';
		if ( in_array( $typenow, wc_get_order_types( 'order-meta-boxes' ), true ) ) {
			echo '<input type="text" name="_voucher" value="' . esc_attr( $voucher ) . '" placeholder="Voucher"/>';
		}
	}
}