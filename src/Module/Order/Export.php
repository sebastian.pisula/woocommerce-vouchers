<?php

namespace IC\WooCommerce\Voucher\Module\Order;

use IC\WooCommerce\Voucher\Lib\Helper;
use ParseCsv\Csv;

/**
 * Class Order_List
 *
 * @package IC\WooCommerce\Voucher\Module
 */
class Export {
	/**
	 * Register hooks.
	 */
	public function add_hooks() {
		add_filter( 'bulk_actions-edit-shop_order', [ $this, 'add_bulk_action' ], 100 );
		add_filter( 'handle_bulk_actions-edit-shop_order', [ $this, 'handle_bulk_action' ], 100, 3 );
	}

	/**
	 * @param array $actions
	 *
	 * @return array
	 */
	public function add_bulk_action( $actions ) {
		$actions['voucher_export'] = 'Export vouchers to CSV';

		return $actions;
	}

	public function handle_bulk_action( $redirect_to, $action, $ids ) {
		if ( $action !== 'voucher_export' ) {
			return $redirect_to;
		}

		set_time_limit( 0 );

		$data = [];

		foreach ( $ids AS $order_id ) {
			$vouchers = Helper::get_order_vouchers( $order_id );

			foreach ( $vouchers AS $item_id => $codes ) {
				foreach ( $codes AS $code ) {
					$data[] = [
						'order_id' => $order_id,
						'product'  => Helper::get_line_item( $item_id )->get_product()->get_title(),
						'code'     => $code,
					];
				}
			}
		}

		if ( ! $data ) {
			return $redirect_to;
		}

		$csv = new Csv();

		$headers = [
			'Order ID',
			'Product Name',
			'Code',
		];

		$csv->output( 'vouchers.csv', $data, $headers );

		die();
	}
}