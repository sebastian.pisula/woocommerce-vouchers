<?php

namespace IC\WooCommerce\Voucher\Module\Order;

use Exception;
use IC\WooCommerce\Voucher\Lib\Helper;
use Mpdf\Mpdf;
use Mpdf\MpdfException;
use Mpdf\Output\Destination;
use WC_Meta_Data;
use WC_Order;
use WC_Order_Item;
use WC_Order_Item_Product;

/**
 * Class Detail
 *
 * @package IC\WooCommerce\Voucher\Module
 */
class Detail {
	/**
	 * Register hooks.
	 */
	public function add_hooks() {
		//Download PDF
		add_filter( 'query_vars', [ $this, 'add_query_vars' ] );
		add_action( 'template_redirect', [ $this, 'download_pdf' ] );
		add_filter( 'rewrite_rules_array', [ $this, 'add_rewrite_rules' ] );

		add_action( 'woocommerce_order_status_completed', [ $this, 'generate_voucher_codes' ], 10, 2 );
		add_filter( 'woocommerce_order_item_display_meta_key', [ $this, 'change_voucher_keys' ], 20, 3 );
		add_filter( 'woocommerce_order_item_display_meta_value', [ $this, 'change_voucher_values' ], 10, 3 );

		add_filter( 'woocommerce_email_attachments', [ $this, 'woocommerce_email_attachments' ], 10, 4 );

		add_action( 'woocommerce_order_item_meta_end', [ $this, 'add_item_meta' ], 10, 3 );
	}

	/**
	 * @param int                   $item_id
	 * @param WC_Order_Item_Product $item
	 * @param WC_Order              $order
	 */
	public function add_item_meta( $item_id, $item, $order ) {
		if ( get_option( 'wc_vouchers_sent_as', 'link' ) !== 'link' ) {
			return;
		}

		$vouchers = Helper::get_order_vouchers( $order->get_id() );

		if ( isset( $vouchers[ $item_id ] ) ) {
			foreach ( $vouchers[ $item_id ] AS $code ) {
				echo '<div>';
				$url = $this->get_voucher_url( $item_id, $code );

				echo sprintf( "%s (<a href=\"%s\">Download</a>)", $code, esc_url( $url ) );
				echo '</div>';
			}
		}
	}

	/**
	 * @throws MpdfException
	 */
	public function download_pdf() {
		if ( is_404() ) {
			return;
		}

		if ( get_query_var( 'action' ) !== 'download_voucher' ) {
			return;
		}

		$item_id = (int) get_query_var( 'item_id' );

		try {
			$order_id = wc_get_order_id_by_order_item_id( $item_id );
		} catch ( Exception $e ) {
			trigger_error( $e->getMessage() );

			$this->set_404();

			return;
		}

		$vouchers = Helper::get_order_vouchers( $order_id );

		if ( ! isset( $vouchers[ $item_id ] ) ) {
			$this->set_404();

			return;
		}

		if ( ! in_array( get_query_var( 'code' ), $vouchers[ $item_id ] ) ) {
			$this->set_404();

			return;
		}

		$mpdf = new Mpdf();

		$html = $this->get_voucher_content( $item_id, get_query_var( 'code' ) );

		try {
			$mpdf->WriteHTML( $html );
			$mpdf->Output( 'voucher.pdf', Destination::DOWNLOAD );
			die();
		} catch ( MpdfException $e ) {
			trigger_error( $e->getMessage() );
		}

		$this->set_404();
	}

	/**
	 *
	 */
	private function set_404() {
		global $wp_query;

		$wp_query->set_404();
		status_header( 404 );
	}

	/**
	 * @param array $vars
	 *
	 * @return array
	 */
	public function add_query_vars( $vars ) {
		$vars[] = 'code';
		$vars[] = 'action';
		$vars[] = 'item_id';

		return $vars;
	}

	/**
	 * @param array $rules
	 *
	 * @return array
	 */
	public function add_rewrite_rules( $rules ) {
		$new_rules = [
			'download-voucher/([\d]+)/(.*)/?$' => 'index.php?action=download_voucher&item_id=$matches[1]&code=$matches[2]',
		];

		return $new_rules + $rules;
	}

	public function woocommerce_email_attachments( $attachments, $email_id, WC_Order $order, $email ) {
		if ( get_option( 'wc_vouchers_sent_as' ) !== 'attachment' ) {
			return $attachments;
		}

		if ( $email_id !== 'customer_completed_order' ) {
			return $attachments;
		}

		$status = filter_var( get_post_meta( $order->get_id(), '_has_voucher', 1 ), FILTER_VALIDATE_BOOLEAN );
		if ( ! $status ) {
			return $attachments;
		}

		$vouchers = Helper::get_order_vouchers( $order->get_id() );

		if ( ! $vouchers ) {
			return $attachments;
		}

		foreach ( $vouchers AS $item_id => $line_item ) {
			foreach ( $line_item AS $code ) {
				$html = $this->get_voucher_content( $item_id, $code );

				if ( ! $html ) {
					continue;
				}

				$mpdf = new Mpdf();
				$file = wp_normalize_path( get_temp_dir() . '/' . $order->get_id() . '-' . $code . '.pdf' );

				try {
					$mpdf->WriteHTML( $html );
					$mpdf->Output( $file, Destination::FILE );

					$attachments[] = $file;
				} catch ( MpdfException $e ) {
					trigger_error( $e->getMessage() );
				}
			}
		}

		return $attachments;
	}

	/**
	 * @param string        $key
	 * @param WC_Meta_Data  $meta
	 * @param WC_Order_Item $item
	 *
	 * @return string|void
	 */
	public function change_voucher_keys( $key, $meta, $item ) {
		if ( '_voucher' === $meta->key ) {
			return 'Voucher Code';
		}

		return $key;
	}

	/**
	 * @param string                $display_value
	 * @param WC_Meta_Data          $meta
	 * @param WC_Order_Item_Product $item
	 *
	 * @return string
	 */
	public function change_voucher_values( $display_value, WC_Meta_Data $meta, $item ) {
		if ( '_voucher' === $meta->key ) {
			$url = $this->get_voucher_url( $item->get_id(), $meta->value );

			return $display_value . ' (<a href="' . esc_url( $url ) . '">Download</a>)';
		}

		return $display_value;
	}

	private function get_voucher_url( $id, $code ) {
		return home_url( 'download-voucher/' . $id . '/' . $code . '/' );
	}

	/**
	 * @param int      $order_id
	 * @param WC_Order $order
	 */
	public function generate_voucher_codes( $order_id, $order ) {
		$has_voucher = false;

		/** @var WC_Order_Item_Product $item */
		foreach ( $order->get_items() AS $item ) {
			if ( $item->get_product()->get_meta( '_voucher' ) !== 'yes' ) {
				continue;
			}

			if ( $item->meta_exists( '_voucher' ) ) {
				continue;
			}

			$has_voucher = true;

			for ( $i = 1; $i <= $item->get_quantity(); $i ++ ) {
				$item->add_meta_data( '_voucher', mb_strtoupper( wp_generate_password( 12, false ) ) );
			}

			$item->save();
		}

		if ( $has_voucher ) {
			update_post_meta( $order_id, '_has_voucher', 'yes' );
		}
	}

	/**
	 * @param int    $item_id
	 * @param string $code
	 *
	 * @return string
	 */
	private function get_voucher_content( $item_id, $code ) {
		global $post;

		$item        = Helper::get_line_item( $item_id );
		$order       = $item->get_order();
		$product     = $item->get_product();
		$template_id = $product->get_meta( '_voucher_template_id' );

		$post = get_post( $template_id );
		setup_postdata( $post );

		ob_start();

		the_content();

		$html = ob_get_clean();

		$vars = [
			'[voucher_code]'        => $code,
			'[customer_first_name]' => $order->get_billing_first_name(),
			'[last_name]'           => $order->get_billing_last_name(),
			'[product_name]'        => $product->get_title(),
		];

		return str_replace( array_keys( $vars ), $vars, $html );
	}
}