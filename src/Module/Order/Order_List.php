<?php

namespace IC\WooCommerce\Voucher\Module\Order;

use IC\WooCommerce\Voucher\Lib\Helper;

/**
 * Class Order_List
 *
 * @package IC\WooCommerce\Voucher\Module
 */
class Order_List {
	/**
	 * Register hooks.
	 */
	public function add_hooks() {
		add_filter( 'manage_edit-shop_order_columns', [ $this, 'add_new_column' ] );
		add_action( 'manage_shop_order_posts_custom_column', [ $this, 'add_column_content' ] );
		add_action( 'admin_print_styles', [ $this, 'add_admin_styles' ] );
	}

	/**
	 * @param array $columns
	 *
	 * @return array
	 */
	public function add_new_column( $columns ) {
		$new_columns = [];

		foreach ( $columns as $column_name => $column_info ) {

			$new_columns[ $column_name ] = $column_info;

			if ( 'shipping_address' === $column_name ) {
				$new_columns['vouchers'] = 'Vouchers';
			}
		}

		return $new_columns;
	}

	/**
	 * @param string $column
	 */
	public function add_column_content( $column ) {
		global $post;

		if ( 'vouchers' === $column ) {
			$vouchers = Helper::get_order_vouchers( $post->ID );

			foreach ( $vouchers AS $item ) {
				echo implode( '<br />', $item ) . '<br />';
			}
		}
	}

	/**
	 *
	 */
	public function add_admin_styles() {
		wp_add_inline_style( 'woocommerce_admin_styles', '.widefat #vouchers.column-vouchers{ width: 7%; }' );
	}
}