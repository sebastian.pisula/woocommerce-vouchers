<?php

namespace IC\WooCommerce\Voucher\Module\Product\Settings;

use WP_Query;

/**
 * Class Settings
 *
 * @package IC\WooCommerce\Voucher\Module\Product\Settings
 */
abstract class Settings {
	/**
	 * @param int $id
	 *
	 * @return string
	 */
	protected function get_voucher_status( $id ) {
		return get_post_meta( $id, '_voucher', 1 );
	}

	/**
	 * @param int $id
	 *
	 * @return bool
	 */
	protected function has_voucher( $id ) {
		return $this->get_voucher_status( $id ) === 'yes';
	}

	/**
	 * Gets list of templates.
	 *
	 * @return array
	 */
	protected function get_templates() {
		$templates = [ 0 => '-- Wybierz --' ];

		$templates_array = new WP_Query( [
				'post_type' => 'voucher-template',
				'orderby'   => 'post_title',
				'order'     => 'ASC',
				'nopaging'  => true,
				'status'    => 'publish',
			]
		);

		return $templates + wp_list_pluck( $templates_array->posts, 'post_title', 'ID' );
	}

	/**
	 * Gets product/variation voucher template id.
	 *
	 * @param int $id Product ID.
	 *
	 * @return int
	 */
	protected function get_voucher_template( $id ) {
		return (int) get_post_meta( $id, '_voucher_template_id', 1 );
	}
}