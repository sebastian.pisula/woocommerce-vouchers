<?php

namespace IC\WooCommerce\Voucher\Module\Product\Settings;

use WP_Post;

/**
 * Class Variant
 *
 * @package IC\WooCommerce\Voucher\Module\Product\Settings
 */
class Variant extends Settings {
	/**
	 * Register hooks.
	 */
	public function add_hooks() {
		add_action( 'woocommerce_variation_options', [ $this, 'add_voucher_option' ], 10, 3 );
		add_action( 'woocommerce_save_product_variation', [ $this, 'save_product_voucher' ], 10, 2 );
		add_action( 'woocommerce_product_after_variable_attributes', [ $this, 'add_voucher_option_content' ], 10, 3 );
	}

	/**
	 * Adds voucher product type option to variation options.
	 *
	 * @param int     $loop           loop counter
	 * @param array   $variation_data associative array of variation data
	 * @param WP_Post $variation      variation post object
	 */
	public function add_voucher_option( $loop, $variation_data, $variation ) { ?>
		<label>
			<input type="checkbox" class="checkbox variable_has_voucher"
				   name="_voucher[<?php echo $loop; ?>]" <?php checked( $this->has_voucher( $variation->ID ), true ); ?> />
			Voucher
		</label>
		<?php
	}

	/**
	 * Save variation post meta.
	 *
	 * @param int $variation_id
	 * @param int $i
	 */
	public function save_product_voucher( $variation_id, $i ) {
		$has_voucher = isset( $_POST['_voucher'][ $i ] ) ? 'yes' : 'no';

		update_post_meta( $variation_id, '_voucher', $has_voucher );

		if ( 'yes' === $has_voucher && isset( $_POST['_voucher_template_id'][ $i ] ) ) {
			update_post_meta( $variation_id, '_voucher_template_id', (int) $_POST['_voucher_template_id'][ $i ] );
		}
	}

	/**
	 * Displays the voucher select box in the product meta box.
	 *
	 * @param int     $loop           loop counter
	 * @param array   $variation_data associative array of variation data
	 * @param WP_Post $variation      variation post object
	 */
	public function add_voucher_option_content( $loop, $variation_data, WP_Post $variation ) {
		?>
		<div class="show_if_variation_has_voucher <?php if ( ! $this->has_voucher( $variation->ID ) ) : ?>hidden<?php endif; ?>">
			<?php woocommerce_wp_select( [
				'id'            => '_voucher_template_id[' . $loop . ']',
				'label'         => 'Voucher Template',
				'description'   => 'Select a voucher template to make this variation into a voucher product.',
				'wrapper_class' => 'form-row form-row-first',
				'options'       => $this->get_templates(),
				'value'         => $this->get_voucher_template( $variation->ID ),
				'desc_tip'      => true,
			] );
			?>
		</div>
		<?php
	}
}