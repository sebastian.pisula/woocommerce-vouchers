<?php

namespace IC\WooCommerce\Voucher\Module\Product\Settings;

use WC_Product;

/**
 * Class Simple
 *
 * @package IC\WooCommerce\Voucher\Module\Product\Settings
 */
class Simple extends Settings {
	/**
	 * Register hooks.
	 */
	public function add_hooks() {
		add_filter( 'product_type_options', [ $this, 'add_voucher_option' ] );
		add_action( 'woocommerce_process_product_meta', [ $this, 'save_product_voucher' ] );
		add_action( 'woocommerce_product_options_general_product_data', [ $this, 'add_voucher_option_content' ] );
	}

	/**
	 * Adds voucher product type option to simple product options.
	 *
	 * @param array $options
	 *
	 * @return array
	 */
	public function add_voucher_option( $options ) {
		$options['voucher'] = [
			'id'            => '_voucher',
			'wrapper_class' => 'show_if_simple',
			'label'         => 'Voucher',
			'default'       => 'no',
		];

		return $options;
	}

	/**
	 * Save product post meta.
	 *
	 * @param int $post_id
	 */
	public function save_product_voucher( $post_id ) {
		$has_voucher = isset( $_POST['_voucher'] ) ? 'yes' : 'no';

		update_post_meta( $post_id, '_voucher', $has_voucher );

		if ( 'yes' === $has_voucher && isset( $_POST['_voucher_template_id'] ) ) {
			update_post_meta( $post_id, '_voucher_template_id', (int) $_POST['_voucher_template_id'] );
		}
	}

	/**
	 * Displays the voucher select box in the product meta box.
	 */
	public function add_voucher_option_content() {
		/** @var WC_Product $product_object */
		global $product_object;

		if ( $product_object->get_type() === 'simple' ) {
			$status = $this->has_voucher( $product_object->get_id() );
		} else {
			$status = false;
		}

		echo '<div class="options_group show_if_voucher ' . ( $status ? '' : 'hidden' ) . '">';

		woocommerce_wp_select( [
			'id'          => '_voucher_template_id',
			'label'       => 'Voucher Template',
			'description' => '',
			'options'     => $this->get_templates(),
			'desc_tip'    => true,
			'value'       => $this->get_voucher_template( $product_object->get_id() ),
		] );

		echo '</div>';
	}
}