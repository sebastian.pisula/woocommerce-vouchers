<?php

namespace IC\WooCommerce\Voucher\Module\Product;

use IC\WooCommerce\Voucher\Module\Product\Settings\Simple;
use IC\WooCommerce\Voucher\Module\Product\Settings\Variant;

/**
 * Class Settings
 *
 * @package IC\WooCommerce\Voucher\Module\Product
 */
class Settings {
	/**
	 * Register hooks.
	 */
	public function add_hooks() {
		( new Simple() )->add_hooks();
		( new Variant() )->add_hooks();;
	}
}