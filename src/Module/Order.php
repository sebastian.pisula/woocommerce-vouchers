<?php

namespace IC\WooCommerce\Voucher\Module;

use IC\WooCommerce\Voucher\Module\Order\Detail;
use IC\WooCommerce\Voucher\Module\Order\Export;
use IC\WooCommerce\Voucher\Module\Order\Order_List;

/**
 * Class Order
 *
 * @package IC\WooCommerce\Voucher\Module
 */
class Order {
	/**
	 * Register hooks.
	 */
	public function add_hooks() {
		( new Export() )->add_hooks();
		( new Detail() )->add_hooks();
		( new Order_List() )->add_hooks();
	}
}