<?php

namespace IC\WooCommerce\Voucher\Lib;

use Exception;
use WC_Meta_Data;
use WC_Order_Item_Product;

class Helper {
	/**
	 * Get plugin path
	 *
	 * @param string $path
	 *
	 * @return string
	 */
	public static function get_plugin_path( $path = '' ) {
		return wp_normalize_path( plugin_dir_path( WC_V_PLUGIN_FILE ) . '/' . $path );
	}

	/**
	 * @param string $file
	 *
	 * @return string
	 */
	public static function get_plugin_url( $file = '' ) {
		return plugins_url( $file, WC_V_PLUGIN_FILE );
	}

	/**
	 * @param int $order_id
	 *
	 * @return array
	 */
	public static function get_order_vouchers( $order_id ) {
		$order = wc_get_order( $order_id );

		if ( $order->get_meta( '_has_voucher' ) !== 'yes' ) {
			return [];
		}

		$vouchers = [];

		/** @var WC_Order_Item_Product $item */
		foreach ( $order->get_items() AS $item ) {
			if ( ! $item->meta_exists( '_voucher' ) ) {
				continue;
			}

			/** @var WC_Meta_Data Object $meta */
			foreach ( $item->get_meta( '_voucher', false ) AS $meta ) {
				$vouchers[ $item->get_id() ][] = $meta->value;
			}
		}

		return $vouchers;
	}

	/**
	 * @param int $item_id
	 *
	 * @return WC_Order_Item_Product|null
	 */
	public static function get_line_item( $item_id ) {
		try {
			$order_id = wc_get_order_id_by_order_item_id( $item_id );

			/** @var WC_Order_Item_Product $item */
			foreach ( wc_get_order( $order_id )->get_items() AS $item ) {
				if ( $item->get_id() === $item_id ) {
					return $item;
				}
			}
		} catch ( Exception $e ) {
			trigger_error( $e->getMessage() );
		}

		return null;
	}
}