(function ($) {

    var body = $('body');

    body.on('change', ':checkbox[name=_voucher]', function () {
        var elem = $('.options_group.show_if_voucher');

        if ($(this).prop('checked'))
            elem.removeClass('hidden');
        else {
            elem.addClass('hidden');
        }
    });

    body.on('change', ':checkbox.variable_has_voucher', function () {
        var elem = $(this).closest('.woocommerce_variation').find('.show_if_variation_has_voucher');

        if ($(this).prop('checked'))
            elem.removeClass('hidden');
        else {
            elem.addClass('hidden');
        }
    });


})(jQuery);